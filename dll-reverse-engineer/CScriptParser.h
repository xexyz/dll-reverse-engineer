#include <tchar.h>

#ifdef PARSE_EXPORTS
#define PARSE_API __declspec (dllexport)
#else
#define PARSE_API __declspec (dllimport)
#endif


class PARSE_API CScriptParser
{
private:

public:
    CScriptParser (int, int); 

    int Calc (char*);
    int ExecuteLine (char*);

};