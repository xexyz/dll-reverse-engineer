#include <tchar.h>

#ifdef MASTDAT_EXPORTS
#define MASTDAT_API __declspec (dllexport)
#else
#define MASTDAT_API __declspec (dllimport)
#endif


class MASTDAT_API CMasterData
{
private:

public:
	CMasterData (void) {  } 
	
	CMasterData * initsi() { return new CMasterData();}
	int GetCreditTitleAddOn (int);
	int InitMaster(int);
	signed int GetBuildNumber();
	bool IsEval(void);
	void Release(void);


};